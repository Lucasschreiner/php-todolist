<?php
include "../bootstrap/init.php";
if (!isAjaxRequest()) {
    die("Invalid Request");
}
if (isset($_POST["action"]) && !empty($_POST["action"])) {
    switch ($_POST["action"]) {
        case 'addFolder':
            if (strlen($_POST["FolderName"]) < 4) {
                echo "نام پوشه مورد نظر باید بزرگتر از 3 کاراکتر باشد ...";
            }
            echo addFolder($_POST["FolderName"]);
            break;
        case "addTask":

            if (!isset($_POST["folder_id"]) || !is_numeric($_POST["folder_id"])) {
                die("پوشه موردنظر را انتخاب نماببد .");
            }
            $taskName = $_POST["TaskName"];
            $folderId = $_POST["folder_id"];
            echo addTask($taskName, $folderId);
            break;
        default:
            die("Invalid Request");
            break;
    }
}
