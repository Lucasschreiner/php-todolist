<?php defined("BASE_URL") OR die("ACEES DENIDE!");
// Folder Functions
function getAllTasks($folder_id = null)
{
    global $pdo;
    $folder_id = isset($_GET["folder_id"]) ? $_GET["folder_id"] : null;
    $current_user_id = getCurrentUserId();
    $task_conddition = isset($folder_id) ? "and folder_id = $folder_id" : '';
    $query = "select * from tasks where user_id = $current_user_id $task_conddition";
    $stmt = $pdo->prepare($query);
    $stmt->execute();
    $all_tasks = $stmt->fetchAll(PDO::FETCH_OBJ);

    return $all_tasks;
}

function deleteTask($task_id)
{
    global $pdo;
    $query = "delete from tasks where id=$task_id";
    $stmt = $pdo->prepare($query);
    $stmt->execute();
    $QueryRes = $stmt->fetchAll(PDO::FETCH_OBJ);
    return $QueryRes;
}

function addTask($taskName , $folder_id)
{
    global $pdo;
    $current_user_id = getCurrentUserId();
    $query = "insert into tasks (title , user_id , folder_id) values (:title , :user_id , :folder_id)";
    $stmt = $pdo->prepare($query);
    $stmt->execute([":title" => $taskName, ":user_id" => $current_user_id , ":folder_id" => $folder_id]);
    $row = $stmt->fetch(PDO::FETCH_OBJ);
    print_r($row);
}
