<?php defined("BASE_URL") OR die("ACEES DENIDE!");
// Folder Functions
function getAllFolders()
{
    global $pdo;
    $query = "select * from folders";
    $stmt = $pdo->prepare($query);
    $stmt->execute();
    $all_folder = $stmt->fetchAll(PDO::FETCH_OBJ);
    return $all_folder;
}

function deleteFolder($folder_id)
{
    global $pdo;
    $query = "delete from folders where id=$folder_id";
    $stmt = $pdo->prepare($query);
    $stmt->execute();
    $QueryRes = $stmt->fetchAll(PDO::FETCH_OBJ);
    return $QueryRes;
}

function addFolder($folderName)
{
    global $pdo;
    $current_user_id = getCurrentUserId();
    $query = "insert into folders (name , user_id) values (:FolderName , :user_id)";
    $stmt = $pdo->prepare($query);
    $stmt->execute(["FolderName" => $folderName, "user_id" => $current_user_id]);
    $lastInsertId = $pdo->lastInsertId();
    $query = "select id from folders where id = :id";
    $stmt = $pdo->prepare($query);
    $stmt->execute(["id"=>$lastInsertId]);
    $row = $stmt->fetch(PDO::FETCH_OBJ);
    print_r($row->id);
}
