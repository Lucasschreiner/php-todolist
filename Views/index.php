<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <title><?= SITE_TITLE ?></title>
  <link rel="stylesheet" href="assets/css/style.css">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.rtl.min.css">
</head>

<body>
  <!-- partial:index.partial.html -->
  <div class="page">
    <div class="pageHeader">
      <div class="title">Dashboard</div>
      <div class="userPanel"><i class="fa fa-chevron-down"></i><span class="username">John Doe </span>
        <img src="https://s3.amazonaws.com/uifaces/faces/twitter/kolage/73.jpg" width="40" height="40" />
      </div>
    </div>
    <div class="main">
      <div class="nav">
        <div class="searchbox">
          <div><i class="fa fa-search"></i>
            <input type="search" placeholder="Search" />
          </div>
        </div>
        <div class="menu">
          <div class="title">Folders</div>
          <ul class="folder-list">
            <a href="<?= BASE_URL ?>">
              <li class="<?= (!isset($_GET['folder_id'])) ? 'active' : "" ?>">
                <i class="fa fa-dashboard"></i>
                All Tasks
              </li>
            </a>
            <?php foreach ($folders as $folder) : ?>
              <a href="?folder_id=<?= $folder->id ?>">
                <li class="<?= (isset($_GET['folder_id']) && $_GET['folder_id'] == $folder->id) ? 'active' : "" ?>">
                  <i class="fa fa-folder"></i><?= $folder->name ?>
                </li>
              </a>
              <a href="?delete_folder=<?= $folder->id ?>"><i class="fa fa-trash-o folder-trash"></i></a>
            <?php endforeach ?>
          </ul>
          <div class="title" style="margin-bottom: 10px;">Add Folder</div>
          <input type="text" id="add_Folder_Input" class="add-folder-input" placeholder="Add Folder">
          <button class="add-folder-btn" id="Submit_Folder_Data"><i class="fa fa-plus"></i></button>
        </div>
      </div>
      <div class="view">
        <div class="viewHeader">
          <div class="title">Manage Tasks
            <i class="fa fa-list"></i>
            <input type="text" id="addTask" placeholder="Add Your Tasks Name" class="addTasksInput">
          </div>
          <div class="functions">
            <div class="button active">Add New Task</div>
            <div class="button">Completed</div>
            <div class="button inverz"><i class="fa fa-trash-o"></i></div>
          </div>
        </div>
        <div class="content">
          <div class="list">
            <div class="title">Today</div>
            <ul>
              <table class="table table-bordered">
                <?php
                include "task-columns.php";
                if (isset($tasks) && count($tasks) > 0) {
                  foreach ($tasks as $task) {
                    include "task-item.php";
                  }
                } else {
                  include "no-item.php";
                }
                ?>
              </table>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- partial -->
  <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
  <script src="assets/js/script.js"></script>
  <script>
    $(document).ready(function() {
      $("#Submit_Folder_Data").click(function(e) {
        var input = $("input#add_Folder_Input");
        $.ajax({
          url: "proccess/ajax-folder.php",
          method: "post",
          data: {
            action: "addFolder",
            FolderName: input.val()
          },
          success: function(response) {
            $('<a href="?folder-id=' + response + '"><li> <i class="fa fa-folder"></i>' + input.val() + '</li></a><a href="?delete_folder=' + response + '"><i class="fa fa-trash-o folder-trash"></i></a>').appendTo("ul.folder-list");
          },
          error: function(response) {
            alert(response);
          }
        });

      });
    });

    $("#addTask").on('keypress', function(e) {
      if (e.which == 13) {
        $.ajax({
          url: "proccess/ajax-handler.php",
          method: "post",
          data: {
            action: "addTask",
            TaskName: $("#addTask").val(),
            folder_id: <?= $_GET["folder_id"] ?>
          },
          success: function(response) {
            location.reload();
          },
          error: function(response) {
            alert(response);
          }
        })
      }
    });
  </script>
</body>

</html>