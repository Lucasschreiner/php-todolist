<?php defined("BASE_URL") OR die("ACEES DENIDE!"); ?>

<tr class="<?= ($task->is_done == 1) ? 'activate' : ""?>">
    <td><?= $task->id?></td>
    <td><?= $task->title?></td>
    <td><?= $task->user_id?></td>
    <td><?= $task->folder_id?></td>
    <td>
        <input type="checkbox"<?= ($task->is_done == 1) ? 'checked' : ""?> name="isdone" id="isdone">
    </td>
    <td>
        <?php
        include "operation.php";
        ?>
    </td>
</tr>